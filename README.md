# Terminal (Versao 1.0)

## Descricao

Terminal para controlo de faltas de sistema Moderlize.cv criado por Onilio Semedo.

## Instalacao Linux

```
//clone projeto
git clone <repository_url>

// install node.js
sudo apt-get update
sudo apt-get install nodejs
sudo apt-get install npm

//instala dependencia
npm install

//Comecar o terminal
npm install -g pm2

//Caso queres iniciar o terminal a iniciar o dispositivo (optional)
pm2 startup
pm2 save
```

## Configuracao

Via site main via no terminal e cria um novo caso nao tinha criado despois obtem o qrcode e le no terminal configurado e o sistema vai configurar sozinho.


## Author
Onilio Semedo

## License

