//definicao de variaveis
const express = require('express');
const path = require('path');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const port = 3000;
const fs = require('fs');

//path de json de sistema
const jsonFile = 'public/json/data.json';

app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

let jsonData = {};

//acessar json e seus dados
try {
    jsonData = JSON.parse(fs.readFileSync(jsonFile, 'utf8'));
} catch (readError) {
    app.get('/', (req, res) => {
        res.render('erro', { errorMessage: readError.message });
    });
}
//chamda de view
app.get('/', (req, res) => {
    if (jsonData.data.code === 'nDa' || jsonData.data.url === 'nDa' || jsonData.data.status === 'nDa') {
        res.render('configuracao');
    } else if (jsonData.data.status === 'off') {
        res.render('erro', { errorMessage: 'Some error message here' });
    } else {
        res.render('index', { data: jsonData.data });
    }
});
//salvar dados de configuracao no json
app.post('/configuracao', express.json(), (req, res) => {
    const dataNew = req.body;
    Object.assign(jsonData, dataNew);

    fs.writeFile(jsonFile, JSON.stringify(jsonData, null, 2), (writeError) => {
        if (writeError) {
            res.status(500).json({ message: 500 });
        } else {
            res.status(200).json({ message: 200 });
        }
    });
});

io.on('connection', (socket) => {
    console.log('A user connected');
    socket.on('disconnect', () => {
        console.log('User disconnected');
    });
    
});

http.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});